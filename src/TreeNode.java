
import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode(){}

   TreeNode(String n){
      this.name = n;
   }

   TreeNode (String n, TreeNode d, TreeNode r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   //todo: should I leave these even thou they are not used?
   private void setFirstChild(TreeNode firstChild){
      this.firstChild = firstChild;
   }

   private void setNextSibling(TreeNode nextSibling){
      this.nextSibling = nextSibling;
   }


   public boolean hasChild(){
      return this.firstChild != null;
   }

   public boolean hasSibling(){
      return this.nextSibling != null;
   }

   public static TreeNode parsePrefix (String s) {
      s =  s.trim();

      parsePrefixValidation(s);

      TreeNode root;
      if(TreeNode.containsBrackets(s)){

         root = parsePreorder(s);
      }
      else return new TreeNode(s);

      return root;
   }

   //region parsePrefix helper methods

   //the method is based upon the preorder method of SibTreeNode class https://www.cs.berkeley.edu/~jrs/61b/lec/24
   private static TreeNode parsePreorder(String s){

      TreeNode tn = new TreeNode();
      tn.name = getFirstElement(s);

      if(!(s.isEmpty() || s == null)){
         if (parsePrefixNodeHasChildren(s)) {
            tn.firstChild = parsePreorder(parsePrefixGetNodeSubtreePrefix(s));

            int beginningIndexOfSubtreePrefix = parsePrefixFindFirstChildBeginningIndex(s) + 1;
            String child =s.substring(beginningIndexOfSubtreePrefix-1, findFirstBracketsMatchingBracketIndex(s)+1);
            s = s.replace(child,"");
         }
         if (hasNextSibling(s)) {
            int removeFrom = tn.name.length() + 1;
            s = s.substring(removeFrom);
            tn.nextSibling = parsePreorder(s);
         }
      }else {return null;}

      return tn;
   }

   private static boolean hasEvenNumberOfBrackets(String s){
      char[] chars = s.toCharArray();
      int leftBracketNumber = 0;
      int rightBracketNumber = 0;

      for (int i = 0; i<s.length(); i++)
      {
         if(chars[i] == '('){
            leftBracketNumber++;
         }
         if(chars[i] == ')'){
            rightBracketNumber++;
         }
      }

      if(leftBracketNumber - rightBracketNumber != 0)
         return false;

      return true;
   }

   private static boolean rightBracketsMatchLeftBrackets(String s){
      char[] chars = s.toCharArray();
      int leftBracketNumber = 0;
      int rightBracketNumber = 0;

      for (int i = 0; i<s.length(); i++)
      {
         if(chars[i] == '('){
            leftBracketNumber++;
         }
         if(chars[i] == ')'){
            rightBracketNumber++;
         }

         if(leftBracketNumber < rightBracketNumber)
            return false;
      }

      return true;
   }

   private static boolean containsBrackets(String s){
      return s.indexOf('(') > -1 || s.indexOf(')') > -1;
   }

   private static boolean parsePrefixNodeHasChildren(String s){
      int firstChildBeginnigIndex = parsePrefixFindFirstChildBeginningIndex(s);

      if(firstChildBeginnigIndex == -1)
         return false;

      if(s.indexOf(",") != -1 && s.indexOf(',')<firstChildBeginnigIndex)
         return false;

      return true;
   }

   private static boolean hasNextSibling(String s) {

      int fisrtElementEndIndex =getFirstElementEndIndex(s);

      if(fisrtElementEndIndex==-1)
         return false;

      if(s.charAt(fisrtElementEndIndex) == ',')
         return true;

      if(parsePrefixNodeHasChildren(s)){
         int firstBracketEnd = findFirstBracketsMatchingBracketIndex(s);
         if(firstBracketEnd+1 < s.length() && s.charAt(firstBracketEnd+1) == ','){
            return true;
         }
      }

      return false;
   }

   private static String parsePrefixGetNodeSubtreePrefix(String s){
      int beginningIndexOfSubtreePrefix = parsePrefixFindFirstChildBeginningIndex(s) + 1;
      return s.substring(beginningIndexOfSubtreePrefix, findFirstBracketsMatchingBracketIndex(s));
   }

   private static String getFirstElement(String s){
      int firstChildBeginnigIndex = parsePrefixFindFirstChildBeginningIndex(s);
      int endOfFirstElement = s.indexOf(',');

      if(endOfFirstElement == firstChildBeginnigIndex) { //both elements are -1
         return s;
      }
      if(endOfFirstElement != -1 && endOfFirstElement<firstChildBeginnigIndex){
         return s.substring(0,endOfFirstElement);
      }


      if(firstChildBeginnigIndex != -1 && firstChildBeginnigIndex < endOfFirstElement)
      {
         return s.substring(0,firstChildBeginnigIndex);
      }

      if(endOfFirstElement == -1){
         return s.substring(0,firstChildBeginnigIndex);
      }

      else{
         return s.substring(0,endOfFirstElement);
      }
   }

   private static int getFirstElementEndIndex(String s){
      int firstChildBeginnigIndex = parsePrefixFindFirstChildBeginningIndex(s);
      int endOfFirstElement = s.indexOf(',');

      if(endOfFirstElement == firstChildBeginnigIndex) { //both elements are -1
         return -1;
      }
      if(endOfFirstElement != -1 && endOfFirstElement<firstChildBeginnigIndex){
         return endOfFirstElement;
      }

      if(firstChildBeginnigIndex != -1 && firstChildBeginnigIndex < endOfFirstElement)
      {
         return firstChildBeginnigIndex;
      }

      if(endOfFirstElement != -1){
         return endOfFirstElement;
      }

      else{
         return firstChildBeginnigIndex;
      }
   }

   private static int parsePrefixFindFirstChildBeginningIndex(String s){
      return s.indexOf('(');
   }

   private static int findFirstBracketsMatchingBracketIndex(String s){
      char[] chars = s.toCharArray();
      int leftBracketNumber = 0;
      int rightBracketNumber = 0;
      int i = 0;

      do
      {
         if(chars[i] == '('){
            leftBracketNumber++;
         }
         if(chars[i] == ')'){
            rightBracketNumber++;
         }
         if(leftBracketNumber == 0 && rightBracketNumber > 0)
            throw new RuntimeException("Invalid input:" + s);

         i++;
      }while (leftBracketNumber != rightBracketNumber || leftBracketNumber == 0);

      return i-1;
   }

   private static void parsePrefixValidation(String s) {
      if(s.isEmpty() || s == null)
         throw new RuntimeException("A prefix representation of a tree can not be empty.");

      if(s.contains("()"))
         throw new RuntimeException("Can not contain empty child. input:" + s);

      if(s.contains("(("))
         throw new RuntimeException("Invalid input: " + s + " can not contain double left bracket.");

      if(s.contains(" "))
         throw new RuntimeException("Can not contain space. input:" + s);

      if(s.contains("(,") || s.contains(",(") || s.contains(",,"))
         throw new RuntimeException("Can not contain illigal sequence ,( or (, or ,,. Input:" + s);

      if(s.contains(",") && !s.contains("("))
         throw new RuntimeException("A tree root can not have a sibling. Input: " + s);

      if(hasNextSibling(s) && findFirstBracketsMatchingBracketIndex(s) < s.length()-1)
         throw new RuntimeException("A tree root can not have a sibling. Input: " + s);

      if(!hasEvenNumberOfBrackets(s))
         throw new RuntimeException("There are an uneven number of brackets in the input:" + s);

      if(!rightBracketsMatchLeftBrackets(s))
         throw new RuntimeException("Right brackets don't match left brackets in input:" + s);
   }
   //endregion

   //the method is based upon the preorder method of SibTreeNode class https://www.cs.berkeley.edu/~jrs/61b/lec/24
   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();

      if(this.hasChild()){
         b.append("(");
         b.append(this.firstChild.rightParentheticRepresentation());
         b.append(")");
         b.append(this.name);
      }
      else
         b.append(this.name);

      if(this.hasSibling()){
         b.append(",");
         b.append(this.nextSibling.rightParentheticRepresentation());

      }

      return b.toString();
   }

   public static void main (String[] param) {

      TreeNode tn = new TreeNode("kiisu");

      System.out.println(tn.name + ";");

      String s = "B1(C(k,f(s,x),t(l,m)),D)";

      TreeNode tn2 = TreeNode.parsePrefix(s);

      System.out.println(tn2.rightParentheticRepresentation());

      s = "A(B1,C,D)";
      TreeNode t = TreeNode.parsePrefix (s);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}

